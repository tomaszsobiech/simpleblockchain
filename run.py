import datetime
import hashlib
import copy

from flask import Flask
from flask import jsonify
from flask import request

import binascii
import base58

import Crypto.Random
from Crypto.Hash import SHA256, RIPEMD160
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5


MINING_REWARD_ADDRESS = 'MINING_REWARD_ADDRESS'

class Transaction:
    def __init__(self, sender_address, receiver_address, amount, private_key):
        """
        From/to addresses are public key of the wallets
        :param sender_address:
        :param receiver_address:
        """
        self.sender_address = sender_address
        self.receiver_address = receiver_address
        self.amount = amount

        # Poniższe wrzucić podczas generowania portfela.

        #private_key = private_key #private_key_1
        # powyższe wrzucić podczas generowania portfela

        self.sender_private_key = private_key

        self.signature = self.sign_transaction()

    def transaction_to_dict(self):
        result = {
            'sender_address': self.sender_address,
            'receiver_address': self.receiver_address,
            'amount': self.amount,
        }

        return result

    def sign_transaction(self):
        """
        Sign my transaction by using private key
        :return:
        """
        self.sender_private_key = binascii.hexlify(self.sender_private_key.exportKey(format='DER'))#.decode('ascii')
        private_key = RSA.import_key(binascii.unhexlify(self.sender_private_key))
        # made signer bu using private key
        signer = PKCS1_v1_5.new(private_key)
        # my message in SHA256 (funkcja hashująca/funkcja skrótu)
        cipher = SHA256.new(str(self.transaction_to_dict()).encode('utf8'))

        # signing the cipher (podpisywanie sunkcji skrótu)
        result = binascii.hexlify(signer.sign(cipher))
        return result


class Block:

    def __init__(self, index, transaction_list, previous_hash=''):
        self.index = index
        self.timestamp = datetime.datetime.now()
        self.transaction_list = transaction_list
        self.previous_hash = previous_hash
        self.nonce = 0  # Using for proof of work (0, 00, 000 at the beginning of hash)
        self.block_hash = self.create_block_hash()

    def create_block_hash(self):
        block_string = (str(self.index) +
                        str(self.timestamp) +
                        str(self.transaction_list[-1].amount) +
                        str(self.previous_hash) +
                        str(self.nonce)).encode()

        return SHA256.new(block_string).hexdigest()  # (funkcja hashująca/funkcja skrótu)

    def mineBlock(self, difficulty):
        """
        Make sure that hash starts with few zeros. If difficulty is 3 hash starts with 3 zeros: e.g. 0x000abcdef...
        :param difficulty:
        :return:
        """
        while self.block_hash[:difficulty] != '0'*difficulty:  # Checking if the beginning of hash is e.g. 0x000...
            self.nonce += 1
            self.block_hash = self.create_block_hash()

        response = {
            'message': 'New block mined',
            'block_index': self.index,
            'block_hash': self.block_hash,
            'transaction_list':
                [
                    transaction.transaction_to_dict() for transaction in self.transaction_list
                ],
            'nonce': self.nonce,
            'previous_hash': self.previous_hash
        }

        return response

    # def __str__(self):
    #     return 'Block index: ' + str(self.index) + '\nHash: ' + str(self.block_hash) + \
    #            '\ntransaction_list: ' + str(self.transaction_list) + '\nDatetime: ' + str(self.timestamp) + '\n\r'

    # def display_chain_dict(self):
    #     result = {
    #         'transactionlist': self.transaction_list
    #
    #     }
    #     return result


class Blockchain:
    def __init__(self, diff):
        block = self.create_first_block()
        self.blockchain = [copy.copy(block)]
        self.difficulty = diff
        self.pendingTransactions = []
        self.mining_reward = 10

    def create_first_block(self):
        random_gen = Crypto.Random.new().read
        private_key_first_block = RSA.generate(1024, random_gen)

        return Block(0, [Transaction(None, None, 0, private_key_first_block)])

    def get_last_Block(self):
        return self.blockchain[-1]

    def mine_pendingTransaction(self, mining_reward_address):
        """
        Mining block function.
        Mining all pending transaction and send the reward to miner.
        Miner get the reward (on wallet) when next block will be mined.
        :param mining_reward_address:
        :return:
        """

        newBlock = Block(0, self.pendingTransactions)
        newBlock.previous_hash = self.get_last_Block().block_hash
        newBlock.index = self.get_last_Block().index + 1
        result = newBlock.mineBlock(self.difficulty)
        #print('Block is mined! Reward = ', self.mining_reward)
        self.blockchain.append(copy.copy(newBlock))
        self.pendingTransactions = [Transaction(MINING_REWARD_ADDRESS, mining_reward_address, self.mining_reward, private_key_first_block)]
        return result

    def blockchain_to_dict(self):
        blockchain_list = []

        for block in blockchain.blockchain:
            #blockchain_dict.append({'index': block.index, 'block_hash': block.block_hash, 'prev_block_hash': block.previous_hash,'transaction_list': block.transaction_list})
            blockchain_list.append(
                    {
                        'index': block.index,
                        'block_hash': block.block_hash,
                        'prev_block_hash': block.previous_hash,
                        'transaction_list':
                            [
                                transaction.transaction_to_dict() for transaction in block.transaction_list
                            ]
                    }
            )
        blockchain_dict = {
            "blockchain length": len(self.blockchain),
            "blockchain": blockchain_list
        }
        return blockchain_dict

    def verify_transaction(self, transaction):
        """
        Checking if the signature of transaction could be made using the private key.
        :param transaction:
        :return:
        """

        private_key = RSA.importKey(binascii.unhexlify(transaction.sender_private_key))
        public_key = private_key.publickey()

        verifier = PKCS1_v1_5.new(public_key)

        # funkcja hashująca/funkcja skrótu
        cipher = SHA256.new(str(transaction.transaction_to_dict()).encode('utf8'))

        # checking signature:
        return verifier.verify(cipher, binascii.unhexlify(transaction.signature))


    def is_Transaction_added_to_blockchain(self, transaction):
        if transaction.sender_address == MINING_REWARD_ADDRESS:
            self.pendingTransactions.append(transaction)
        else:
            if self.verify_transaction(transaction):
                self.pendingTransactions.append(transaction)
                print('Dodano transakcje do bloku')
                return 1
            else:
                print('Nie dodano transakcji do bloku')
                return 0

    def get_balance(self, address):
        balance = 0
        for b in self.blockchain:
            for t in b.transaction_list:
                if t.receiver_address == address:
                    balance += t.amount
                if t.sender_address == address:
                    balance -= t.amount
        return balance

    def is_new_block_valid(self, new_block):
        current_block = self.blockchain[-1]
        if current_block.index + 1 != new_block.index:
            print('Invalid block!')
            return False
        if current_block.block_hash != new_block.previous_hash:
            print('Invalid block!')
            return False
        if new_block.block_hash != new_block.create_block_hash():
            print('Invalid block!')
            return False
        return True


    def is_chain_valid(self):
        """
        Check if a blockchain is valid
        :return: True or False
        """
        for i in range(1, len(self.blockchain)):  # from first block!
            prev_block = self.blockchain[i - 1]
            current_block = self.blockchain[i]

            if current_block.block_hash != current_block.create_block_hash():
                print('Invalid block')
                return False
            if current_block.previous_hash != prev_block.block_hash:
                print('Invalid chain')
                return False
        return True

# working but still not using
def create_wallet_address(public_key_str):

    public_key_str = '04' + public_key_str
    public_key_utf8 = public_key_str.encode('utf8')
    ripmd160 = RIPEMD160.new(SHA256.new(public_key_utf8).digest())
    binary_addr = ripmd160.digest()
    binary_addr_with_my_prefix = bytes.fromhex('50') + binary_addr  # prefix 50 - address starts from 'Z'
    checksum = SHA256.new(SHA256.new(binary_addr_with_my_prefix).digest()).digest()[:4]
    addr = base58.b58encode(binary_addr_with_my_prefix + checksum)
    return addr


if __name__ == '__main__':
    DIFFICULTY = 1
    blockchain = Blockchain(diff=DIFFICULTY)


    # blockchain.createTransaction(Transaction(address_1, address_2, 1, private_key_test))
    # blockchain.createTransaction(Transaction(address_2, address_1, 2, private_key_test))
    #
    # print('start mining')
    # blockchain.mine_pendingTransaction('address')
    #
    # blockchain.createTransaction(Transaction(address_1, address_2, 3, private_key_test))
    # blockchain.createTransaction(Transaction(address_2, address_1, 4, private_key_test))
    #
    # print('start mining')
    # blockchain.mine_pendingTransaction('address_minera')
    #
    # blockchain.createTransaction(Transaction(address_1, address_2, 5, private_key_test))
    # blockchain.createTransaction(Transaction(address_2, address_1, 6, private_key_test))


    app = Flask(__name__)

    @app.route('/mine', methods=['GET'])
    def mine():

        return jsonify(blockchain.mine_pendingTransaction('address_minera')), 200

    @app.route('/transactions/new', methods=['POST'])
    def new_transaction():
        values = request.json

        required = ['sender_address', 'receiver_address', 'amount', 'private_key']

        if sorted(list(values.keys())) != sorted(required):
            return 'Missing values', 400
        private_key_string = values['private_key']
        private_key = RSA.importKey(binascii.unhexlify(private_key_string))

        transaction_result = blockchain.is_Transaction_added_to_blockchain(
                                                Transaction(values['sender_address'],
                                                values['receiver_address'],
                                                values['amount'], private_key))

        if transaction_result:
            response = {'message': 'Transaction added to the last Block'}
        else:
            response = {'message': 'Transaction added failed'}
            return jsonify(response), 500
        return jsonify(response), 201

    @app.route('/chain', methods=['GET'])
    def display_blockchain():
        response = blockchain.blockchain_to_dict()
        return jsonify(response), 200

    @app.route('/')
    def hello():
        return 'Simple blockchain'

    @app.route('/wallet/new')
    def new_wallet():
        random_gen = Crypto.Random.new().read
        private_key = RSA.generate(1024, random_gen)
        public_key = private_key.publickey()
        public_key_str = binascii.hexlify(public_key.exportKey(format='DER')).decode('ascii')
        wallet_address = create_wallet_address(public_key_str)

        response = {
            'message': 'Please save it.',
            'wallet_address': wallet_address.decode('ascii'),
            'private_key': binascii.hexlify(private_key.exportKey(format='DER')).decode('ascii'),
            'public_key': binascii.hexlify(public_key.exportKey(format='DER')).decode('ascii'),
        }

        print('New wallet address: ', wallet_address.decode('ascii'))
        return jsonify(response), 200

if __name__ == '__main__':
    app.run()