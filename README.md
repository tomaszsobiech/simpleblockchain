# SIMPLE BLOCKCHAIN PROJECT
[Some information about blockchain](https://en.wikipedia.org/wiki/Blockchain)

How blockchain works:
![](img/graph.png)

graph source: [What is blockchain](https://bitcoinira.com/what-is-blockchain)

The structure of transaction in a Bitcoin blockchain:
![](img/graph_transaction.png)

transaction graph source: [Transaction graph](https://www.researchgate.net/figure/The-structure-of-transaction-in-a-Bitcoin-blockchain_fig1_324791073)

Still working on it.

What done:
- Block class
- Blockchain class
- checking new block is valid
- checking whole chain is valid
- mining
- reward for mining
- transaction
- flask (basic)
- verify_transaction_signature and adding to the block
- wallet added (wallet address like bitcoin (starts by 'Z' not by '1' or '3'))

TODO:
- add requirements
- nodes
- add step by step how to run
- sender address veryfication?
